import subprocess

def port_macro(value, port_type):
    if port_type == 'tims':
        return f"PORT_TIMER_{value.upper()}"
    return f"PORT_{port_type[:-1].upper()}_{value.upper()}"

def handler_name(value):
    return f"On{value.capitalize()}"

def sender_name(value):
    return f"Send{value.capitalize()}"

def generate_capnp_id(value):
    cmd = ['capnp', 'id']
    id = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    return id.decode("utf-8").rstrip()

def handler_message_type(value, port_type):
    type_name = 'type'
    if port_type == 'tims':
        return ""
    elif port_type in ['clts', 'qrys', 'reqs']:
        type_name = 'rep_type'
    elif port_type in ['anss', 'reps', 'srvs']:
        type_name = 'req_type'
    return value[type_name]

def sender_message_type(value, port_type):
    type_name = 'type'
    if port_type == 'tims':
        return ""
    elif port_type in ['clts', 'qrys', 'reqs']:
        type_name = 'req_type'
    elif port_type in ['anss', 'reps', 'srvs']:
        type_name = 'rep_type'
    return value[type_name]

def handler_params(value, port_type):
    msg_type = handler_message_type(value, port_type)
    if msg_type == "":
        return ""
    return f"messages::{msg_type}::Reader& message, "