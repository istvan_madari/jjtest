{% extends "comp.common.tpl" %}

{% block macros %}
#ifndef {{element.name|upper}}BASE_H
#define {{element.name|upper}}BASE_H
{% endblock macros %}

{% block includes %}
#include <pybind11/stl.h>
#include <pybind11/pybind11.h>
#include <componentmodel/r_componentbase.h>
#include <messages/{{element.appname|lower}}.capnp.h>

namespace py = pybind11;
{% endblock includes %}

{% block portdefinitions %}
{{ macros.portdefine(element.ports) }}
{% endblock portdefinitions %}

{% block component %}
        class {{baseclassname}} : public riaps::ComponentBase {
        public:
            {{baseclassname}}(const py::object*  parent_actor     ,
                          const py::dict     actor_spec       ,
                          const py::dict     type_spec        ,
                          const std::string& name             ,
                          const std::string& type_name        ,
                          const py::dict     args             ,
                          const std::string& application_name ,
                          const std::string& actor_name       );


{% for port_type, value in element.ports.items() %}
{% if value %}
{% if port_type in macros.handler_ports %}
{% for port_name, port_params in value.items() %}
            virtual void {{ port_name|handlername }}({{port_params|handlerparams(port_type)}}riaps::ports::PortBase* port)=0;
{% endfor %}
{% endif %}
{% if  port_type in macros.sender_ports %}
{% for port_name, port_params in value.items() %}
            virtual bool {{ port_name|sendername }}(capnp::MallocMessageBuilder& messageBuilder, messages::{{port_params|sendermessagetype(port_type)}}::Builder& message) final;
{% endfor %}
{% endif %}
{% endif %}
{% endfor %}

            virtual ~{{baseclassname}}() = default;
        protected:
            virtual void DispatchMessage(capnp::FlatArrayMessageReader* capnpreader,
                                         riaps::ports::PortBase*   port,
                                         std::shared_ptr<riaps::MessageParams> params) final;

            virtual void DispatchInsideMessage(zmsg_t* zmsg, riaps::ports::PortBase* port) final;
        };
{% endblock component %}

{% block endmacros %}
#endif // {{element.name|upper}}BASE_H
{% endblock endmacros %}